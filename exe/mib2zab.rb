#!/usr/bin/env ruby
require 'snmp/smi'
require 'zabbix/api'
require 'optimist'
require 'amazing_print'
require 'pry'



opts = Optimist::options do
  opt :interval, "Default polling interval in seconds", type: :integer, default: 60
  opt :mib, "Mib to load", type: :string, required: true
  opt :url, "URL up to but no including api_jsonrpc.php",type: :string,default:'http://localhost'
  opt :user, "User name to authenticate", type: :string, required: true
  opt :pass, "Pass to auth user with", type: :string, required: true
  opt :createstuff, "Do not actually make API calls to create stuff", type: :boolean, default: true
end


def getGroupId(aGroupName)
  @api.hostgroup.get(filter: {name: aGroupName}).first.groupid
end


ZabbixSNMPAgentItem = 20

ZabbixTypemap = {
  float: 0,
  character: 1,
  integer: 3,
  string: 4
}
ZabbixRevTypemap = ZabbixTypemap.invert

def zabbixValueType(anSnmpBasetype)
  case anSnmpBasetype
  when /integer|timeticks/i
    ZabbixTypemap[:integer]
  when /float/i
    ZabbixTypemap[:float]
  when /octet/i
    ZabbixTypemap[:string]
  else
    ZabbixTypemap[:float]
  end
end

def zabbixPreprocessing(anSnmpBasetype)
  # At some point I need to detect counters vs gauges 
  # etc and return appropriate preprocessing params
  # for item creation
end


Zabbix::Api::OpenStruct.prettymode = true

@api = Zabbix::Api::Client.new(url: opts[:url])
@api.login(user: opts[:user],pass:opts[:pass])

mib = SNMP::SMI::MIB.load(opts[:mib])

template_groupid = getGroupId('Templates')

templateid = @api.template.create(host:"A New One", groups: [groupid: template_groupid]).result['templateids'].first if opts[:createstuff]

# This sets up the regular non-lld values
mib[opts[:mib]].topscalars.values.each {|scalar|
  basetype = mib[opts[:mib]].findBasetype(scalar.type)
  zabtype = zabbixValueType(basetype)
  puts "#{scalar.name}, #{scalar.oid}, #{scalar.type} -> #{ZabbixRevTypemap[zabtype]}"
  @api.item.create(hostid: templateid, name: scalar.name, key_: scalar.name, type: ZabbixSNMPAgentItem, value_type: zabtype,delay: opts[:interval],snmp_oid: scalar.oid,description: scalar.description) if opts[:createstuff]
}

# This sets up all the LLD stuff
mib[opts[:mib]].tables.values.each {|table|
  # Each table is potentially a new discovery rule. However
  # if there are tables that are indexed by the same OID,
  # we want to fold those all together into one disco rule.
  # So we need to build up a structure first and see if any
  # of that combination logic applies.
  table.rows.values.each {|row|
    ruleindex = Hash.new(nil)
    mib[row.linkage.values.first.module].tap {|mod|
      mod.scalars[row.linkage.values.first.name].tap {|scalar|
        if scalar.type == :linkage
          mod.scalars[scalar.linkage.values.first.name].tap {|linkedscalar|
            ruleindex[:linkedfrom] = row.name
            ruleindex[:oid] = linkedscalar.oid
            ruleindex[:name] = linkedscalar.name
            ruleindex[:type] = linkedscalar.type
            ruleindex[:description] = linkedscalar.description
            ruleindex[:basetype] = mod.findBasetype(linkedscalar.type) 
          }
        else
          ruleindex[:oid] = scalar.oid
          ruleindex[:name] = scalar.name
          ruleindex[:type] = scalar.type
          ruleindex[:description] = scalar.description
          ruleindex[:basetype] = mod.findBasetype(scalar.type)
        end
      }
      puts "-"*80
      puts row.name
      puts row.description
      print "Rule index: "
      ap ruleindex
      print "Available columns: "
      binding.pry
      ap Hash[*row.columns.values.select {|each| mod.findBasetype(each.type) =~ /octet/i}.collect {|each| [each.name,each.oid]}.flatten]
    } 
    row.columns.values.each {|column|
    }
  }
}

@api.template.delete([templateid]) if opts[:createstuff]

print "user.logout: "
ap @api.logout
