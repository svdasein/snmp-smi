require 'set'
module SNMP
module SMI
  Asn1BaseTypes = Set.new('INTEGER,OCTET STRING,OBJECT IDENTIFIER'.split(','))
  AppDefinedTypes = Set.new('Integer32,IpAddress,Counter32,Gauge32,Unsigned32,TimeTicks,Opaque,Counter64'.split(','))
  BaseTypes = Asn1BaseTypes + AppDefinedTypes
end
end
