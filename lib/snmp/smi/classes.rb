require 'nokogiri'
module SNMP
module SMI
  # Structure of Management Information rfc2578

  class MIB
    # class methods
    def self.load(aMibList)
      aMibList = [aMibList] if aMibList.class != Array
      @loadedMibs = Hash.new if not @loadedMibs
      @loadedMibs.merge!(Hash[*aMibList.collect{|each| [each, SMI::MIB.new(each)]}.flatten])
      self
    end
    def self.module(aMibName)
      @loadedMibs[aMibName]
    end
    def self.[](aString)
      @loadedMibs[aString]
    end
    def self.modules
      @loadedMibs
    end

    # instance methods
    attr_reader :doc,:module,:imports,:typedefs,:traps,:groups,:compliances,:topscalars,:tables,:scalars
    def initialize(aMibName)

      # NOTE: smidump is a bit weird about how it searches the SMIPATH (or paths in /etc/smi.conf):
      # it presumes that if there's an extension to the file names, that they're lower case only.
      # It searches all kinds of permutations of extensions, but will only look at lowercase.
      # So to make it work, just rename all your mib files to have a .txt extension.  It doesn't
      # affect snmp logic really as the filenames are mostly meaningless - it's the module name
      # definition inside that counts. Also, the -k switch means "keep going" - it's more
      # lenient towards formatting errors etc.

      @doc = Nokogiri::XML(`smidump -k -f xml #{aMibName}`)
      @module = Module.new(@doc.xpath('/smi/module'))
      @imports = Import.slurp(@doc.xpath('/smi/imports/import'))
      @typedefs = Typedef.slurp(@doc.xpath('/smi/typedefs/typedef'))
      @traps = Trap.slurp(@doc.xpath('/smi/notifications/notification'))
      @groups = Group.slurp(@doc.xpath('/smi/groups/group'))
      @compliances = Compliance.slurp(@doc.xpath('/smi/compliances/compliance'))
      @topscalars = Scalar.slurp(@doc.xpath('/smi/nodes/scalar'))
      @tables = Table.slurp(@doc.xpath('/smi/nodes/table'))
      @scalars = Hash[*(@topscalars.values + @tables.values.collect{|table| table.scalars}.flatten).map {|scalar| [scalar.name,scalar]}.flatten]
    end

    def findBasetype(aType)
      @typedefs.has_key?(aType) ? @typedefs[aType].basetype : aType
    end

    def inspect
      %Q(#{@module.inspect})
    end
  end

  class Object
    attr_reader :doc,:name

    def self.slurp(aList)
      Hash[*aList.collect { |each| self.new(each)}.map {|each| [each.name,each]}.flatten]
    end

    def initialize(aDoc)
      @doc= aDoc
      @name = @doc.xpath('@name').to_s
    end

    def inspect
      %Q(class: #{self.class} name: #{@name})
    end
  end

  class Import < Object
    attr_reader :module
    def initialize(*)
      super
      @module = @doc.xpath('@module').to_s
    end
    def inspect
      %Q(#{super} module: #{@module})
    end
  end

  class Module < Object
    attr_reader :description,:identity
    def initialize(*)
      super
      @description = @doc.xpath('description/node()').to_s
      @identity = @doc.xpath('identity/@node').to_s
    end
    def inspect
      %Q(#{super} identity: #{@identity})
    end
  end

  class Typedef < Object
    attr_reader :description,:basetype,:status
    def initialize(*)
      super
      @description = @doc.xpath('description/node()').to_s
      @basetype = @doc.xpath('@basetype').to_s
      @status = @doc.xpath('@status').to_s
      @format = @doc.xpath('format/node()').to_s
      if @format == 'd'
        @range = Range.new(@doc.xpath('range/@min').to_s.to_i,@doc.xpath('range/@max').to_s.to_i)
      elsif @format =~ /a$/
        @range = Range.new(@doc.xpath('range/@min').to_s.to_i.chr,@doc.xpath('range/@max').to_s.to_i.chr)
      end
    end
    def inspect
      %Q(#{super} basetype: #{basetype} format: #{@format} range: #{@range} status: #{@status})
    end
  end

  class Node < Object
    attr_reader :description
    def initialize(*)
      super
      @oid = @doc.xpath('@oid').to_s
      @description = @doc.xpath('description/node()').first.to_s
    end

    def inspect
      %Q(#{super} oid: "#{@oid}")
    end

  end

  class Scalar < Node
    attr_reader :oid,:status,:type,:description,:access
    def initialize(*)
      super
      @status = @doc.xpath('@status').to_s
      @type = [@doc.xpath('syntax/typedef/@basetype').to_s, @doc.xpath('syntax/node()/@name').to_s].detect {|type| type > ''}
      @description = @doc.xpath('description/node()').to_s
      @access = @doc.xpath('access/node()').to_s
      binding.pry if not @type > ''
    end

    def inspect
      %Q(#{super} type: #{@type} access: #{@access} status: #{@status})
    end
  end

  class Table < Node
    attr_reader :rows
    def initialize(*)
      super
      @rows = Row.slurp(@doc.xpath('row'))
    end

    def scalars
      @rows.values.collect {|row| row.columns.values.push(row)}
    end
  end

  class Row < Node
    attr_reader :oid,:columns,:linkage
    def type
      :linkage
    end
    def initialize(*)
      super
      @columns = Scalar.slurp(@doc.xpath('column'))
      @linkage = Linkage.slurp(@doc.xpath('linkage/*'))
    end

    def inspect
      %Q(#{super} linkage: #{@linkage})
    end
  end

  class Linkage < Object
    attr_reader :name,:module
    def initialize(*)
      super
      @name= @doc.xpath('@name').to_s
      @module = @doc.xpath('@module').to_s
    end
    def inspect
      %Q(#{super} module: #{@module})
    end
  end

  class Trap < Node
    attr_reader :objects
    def initialize(*)
      super
      @objects = Import.slurp(@doc.xpath('objects/*'))
    end
    def inspect
      %Q(#{super} objects: #{@objects})
    end
  end

  class Group < Node
    attr_reader :members
    def initialize(*)
      super
      @members = Import.slurp(@doc.xpath('members/*'))
    end
  end

  class Compliance < Node
    def initialize(*)
      super
      # These might be deprecated altogether?
    end
  end

end #module smi
end #module snmp
